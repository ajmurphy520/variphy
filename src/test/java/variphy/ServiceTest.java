package variphy;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import variphy.service.BeerService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Andrew on 12/30/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/main/resources/META-INF/spring/spring-shell-plugin.xml"})
public class ServiceTest {

    @Autowired
    BeerService service;

    @Test
    public void testListAll() {
        String result = service.getAllBeers();
        assertNotNull(result);
        assertEquals(service.getNumberofBeers(), 76);
    }

}
