package variphy.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Andrew on 12/29/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeerList {

    private String message;

    @JsonProperty(value = "data")
    private Beer[] beers;

    public BeerList() {
    }

    public Beer[] getBeers() {
        return beers;
    }

    public void setBeers(Beer[] beers) {
        this.beers = beers;
    }
}
