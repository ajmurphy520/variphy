package variphy.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Andrew on 12/29/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Beer {

    private String name;
    private String id;
    private String description;
    private float abv;
    private float ibu;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss")
    private Date createDate;

    private Style style;

    public Beer() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.trim();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description.trim();
    }

    public float getAbv() {
        return abv;
    }

    public void setAbv(float abv) {
        this.abv = abv;
    }

    public float getIbu() {
        return ibu;
    }

    public void setIbu(float ibu) {
        this.ibu = ibu;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Beer beer = (Beer) o;

        if (!id.equals(beer.id)) return false;
        return !(createDate != null ? !createDate.equals(beer.createDate) : beer.createDate != null);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Name: " + name +
                "\nId: " + id +
                "\nDescription: " + description +
                "\nABV: " + abv +
                "\nIBU: " + ibu +
                "\nCreate Date: " + new SimpleDateFormat("MM/dd/yyyy").format(createDate);
    }
}
