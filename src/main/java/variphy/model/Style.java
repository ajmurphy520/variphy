package variphy.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

/**
 * Created by Andrew on 12/30/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Style {

    private String name;
    private String description;
    private float abvMin;
    private float abvMax;
    private float ibuMin;
    private float ibuMax;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd kk:mm:ss")
    private Date createDate;

    public Style() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getAbvMin() {
        return abvMin;
    }

    public void setAbvMin(float abvMin) {
        this.abvMin = abvMin;
    }

    public float getAbvMax() {
        return abvMax;
    }

    public void setAbvMax(float abvMax) {
        this.abvMax = abvMax;
    }

    public float getIbuMin() {
        return ibuMin;
    }

    public void setIbuMin(float ibuMin) {
        this.ibuMin = ibuMin;
    }

    public float getIbuMax() {
        return ibuMax;
    }

    public void setIbuMax(float ibuMax) {
        this.ibuMax = ibuMax;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
