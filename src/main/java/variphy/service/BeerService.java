package variphy.service;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import variphy.model.Beer;
import variphy.model.BeerList;
import variphy.model.Style;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

/**
 * Created by Andrew on 12/29/2015.
 */
@Component
public class BeerService {

    private static final String API_KEY = "6495f3ff13746e6935cd6e148f69c030";
    private static final String BREWERY_ID = "RzvedX";
    private static final String API_URL_BASE = "http://api.brewerydb.com/v2/";

    private TreeSet<Beer> orderedBeerList;

    public BeerService() {
        processBeerList();
    }

    private BeerList getBeerList() {
        Map<String, String> variables = new HashMap<String, String>();
        variables.put("breweryid", BREWERY_ID);
        variables.put("key", API_KEY);
        RestTemplate template = new RestTemplate();
        return template.getForObject(API_URL_BASE + "brewery/{breweryid}/beers?key={key}", BeerList.class, variables);
    }

    private void processBeerList() {
        long startTime = System.currentTimeMillis();
        BeerList beerList = getBeerList();
        orderedBeerList = new TreeSet<Beer>(new Comparator<Beer>() {
            @Override
            public int compare(Beer o1, Beer o2) {
                int returnValue = o2.getCreateDate().compareTo(o1.getCreateDate());
                if (returnValue == 0) {
                    returnValue = o2.getId().compareTo(o1.getName());
                }
                return returnValue;
            }
        });

        for (Beer currentBeer : beerList.getBeers()) {
            fixEmptyValues(currentBeer);
            orderedBeerList.add(currentBeer);
        }
        System.out.println("Processing took: " + (System.currentTimeMillis() - startTime) + " milliseonds");
    }

    private void fixEmptyValues(Beer currentBeer) {
        if (currentBeer == null || currentBeer.getStyle() == null) {
            return;
        }
        Style currentStyle = currentBeer.getStyle();
        if (currentBeer.getCreateDate() == null) {
            currentBeer.setCreateDate(currentStyle.getCreateDate());
        }
        if (currentBeer.getName() == null) {
            currentBeer.setName(currentStyle.getName());
        }
        if (currentBeer.getDescription() == null) {
            currentBeer.setDescription(currentStyle.getDescription());
        }
        if (currentBeer.getAbv() <= 0.0f && currentStyle.getAbvMin() > 0.0f) {
            currentBeer.setAbv((currentStyle.getAbvMin() + currentStyle.getAbvMax()) / 2.0f);
        }
        if (currentBeer.getIbu() <= 0.0f && currentStyle.getIbuMin() > 0.0f) {
            currentBeer.setIbu((currentStyle.getIbuMin() + currentStyle.getIbuMax()) / 2.0f);
        }
    }

    public int getNumberofBeers() {
        return orderedBeerList.size();
    }

    public String getAllBeers() {
        StringBuilder builder = new StringBuilder();
        for (Beer currentBeer : orderedBeerList) {
            if (builder.length() != 0) {
                builder.append("\n\n");
            }
            builder.append(currentBeer.toString());
        }
        return builder.toString();
    }

    public String getBeerFiltered(float minABV, float maxABV, String searchTerm) {
        StringBuilder builder = new StringBuilder();
        for (Beer currentBeer : orderedBeerList) {
            if (currentBeer.getAbv() >= minABV && currentBeer.getAbv() <= maxABV && currentBeer.getName().contains(searchTerm)) {
                if (builder.length() != 0) {
                    builder.append("\n\n");
                }
                builder.append(currentBeer.toString());
            }
        }
        if (builder.length() == 0) {
            builder.append("No beers found.");
        }
        return builder.toString();
    }
}
