package variphy;

import org.springframework.shell.Bootstrap;

import java.io.IOException;

/**
 * Created by Andrew on 12/29/2015.
 */
public class BeerApplication {

    public static void main(String[] args) throws IOException {
        Bootstrap.main(args);
    }

}
