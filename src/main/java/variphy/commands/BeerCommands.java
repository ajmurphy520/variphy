package variphy.commands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliAvailabilityIndicator;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import variphy.service.BeerService;

/**
 * Created by Andrew on 12/29/2015.
 */
@Component
public class BeerCommands implements CommandMarker {

    @Autowired
    BeerService service;

    @CliAvailabilityIndicator({"beerme"})
    public boolean isCommandAvailable() {
        return true;
    }

    @CliCommand(value = "beerme", help = "Lists beers with specified criteria")
    public String getBeer(
            @CliOption(key = "min", help = "Minimum alcohol by volume. Default of 0.0", unspecifiedDefaultValue = "0.0", specifiedDefaultValue = "0.0")
            final float minABV,
            @CliOption(key = "max", help = "Maximum alcohol by volume. Default of 100.0", unspecifiedDefaultValue = "100.0", specifiedDefaultValue = "100.0")
            final float maxABV,
            @CliOption(key = "search", help = "Displays beers whose names contain value.", unspecifiedDefaultValue = "", specifiedDefaultValue = "")
            final String searchTerm
    ) {
        if (minABV <= 0.0f && maxABV >= 100.0f && searchTerm.length() == 0) {
            return service.getAllBeers();
        }
        else {
            return service.getBeerFiltered(minABV, maxABV, searchTerm);
        }
    }
}
