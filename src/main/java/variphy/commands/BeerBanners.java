package variphy.commands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.shell.plugin.support.DefaultBannerProvider;
import org.springframework.shell.support.util.OsUtils;
import org.springframework.stereotype.Component;
import variphy.service.BeerService;

/**
 * Created by Andrew on 12/29/2015.
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class BeerBanners extends DefaultBannerProvider {

    @Autowired
    BeerService service;

    @Override
    public String getBanner() {
        StringBuilder builder = new StringBuilder();
        builder.append("=======================================" + OsUtils.LINE_SEPARATOR);
        builder.append("*                                     *"+ OsUtils.LINE_SEPARATOR);
        builder.append("*      Beer is good... and stuff      *" +OsUtils.LINE_SEPARATOR);
        builder.append("*                                     *"+ OsUtils.LINE_SEPARATOR);
        builder.append("=======================================" + OsUtils.LINE_SEPARATOR);
        builder.append("Version: ").append(this.getVersion());
        return builder.toString();
    }

    @Override
    public String getVersion() {
        return ".08";
    }

    @Override
    public String getWelcomeMessage() {
        return service.getNumberofBeers() + " beers on the wall.\nLet's go get some beer.";
    }
}
